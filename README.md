Setup
1. Config
     "db": {
    "host": "localhost",
    "dbType": "mysql",
    "port": 3306,
    "user": "root",
    "name": "shopeino",
    "pass": "admin",
    "location": "Asia/Jakarta",
    "sslMode":"disable",
    "alias": "default",
    "force": false,
    "verbose": true,
    "debug": true
  }

2. Database

    -- MySQL dump 10.13  Distrib 5.7.31, for osx10.15 (x86_64)
    --
    -- Host: localhost    Database: shopeino
    -- ------------------------------------------------------
    -- Server version	5.7.31

    /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
    /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
    /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
    /*!40101 SET NAMES utf8 */;
    /*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
    /*!40103 SET TIME_ZONE='+00:00' */;
    /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
    /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
    /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
    /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

    --
    -- Table structure for table `inventory`
    --

    DROP TABLE IF EXISTS `inventory`;
    /*!40101 SET @saved_cs_client     = @@character_set_client */;
    /*!40101 SET character_set_client = utf8 */;
    CREATE TABLE `inventory` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `sku` varchar(100) NOT NULL,
    `name` varchar(100) NOT NULL,
    `price` decimal(10,2) NOT NULL,
    `qty` bigint(20) NOT NULL,
    `promo` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
    /*!40101 SET character_set_client = @saved_cs_client */;

    --
    -- Dumping data for table `inventory`
    --

    LOCK TABLES `inventory` WRITE;
    /*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
    INSERT INTO `inventory` VALUES (1,'120P90','Google Home',49.99,10,2),(2,'43N23P','MacBook Pro',5399.99,5,1),(3,'A304SD','Alexa Speaker',109.50,10,3),(4,'234234','Raspberry Pi B',30.00,2,NULL);
    /*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
    UNLOCK TABLES;

    --
    -- Table structure for table `promo`
    --

    DROP TABLE IF EXISTS `promo`;
    /*!40101 SET @saved_cs_client     = @@character_set_client */;
    /*!40101 SET character_set_client = utf8 */;
    CREATE TABLE `promo` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `promotion` varchar(100) NOT NULL,
    `freeSku` varchar(19) DEFAULT NULL,
    `minqty` int(11) DEFAULT NULL,
    `pay` bigint(20) DEFAULT NULL,
    `percentOff` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
    /*!40101 SET character_set_client = @saved_cs_client */;

    --
    -- Dumping data for table `promo`
    --

    LOCK TABLES `promo` WRITE;
    /*!40000 ALTER TABLE `promo` DISABLE KEYS */;
    INSERT INTO `promo` VALUES (1,'Buy MacBook Pro free Raspberry Pi B','234234',0,0,0),(2,'Buy 3 Google Homes for the price of 2','0',3,2,0),(3,'Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers','0',3,0,10);
    /*!40000 ALTER TABLE `promo` ENABLE KEYS */;
    UNLOCK TABLES;

    --
    -- Dumping routines for database 'shopeino'
    --
    /*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

    /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
    /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
    /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
    /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
    /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
    /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
    /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

    -- Dump completed on 2022-04-14 10:14:46

How to Build

1. go get .
2. go build
3. ./shopeino

How to Test

1. curl --location --request POST 'http://localhost:9042/shopeino/v1/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
    "banyaknya": 1,
    "namaBarang": "Macbook Pro"
}'

2. curl --location --request POST 'http://localhost:9042/shopeino/v1/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
    "banyaknya": 3,
    "namaBarang": "Google Home"
}'

3. curl --location --request POST 'http://localhost:9042/shopeino/v1/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
    "banyaknya": 3,
    "namaBarang": "Alexa Speaker"
}'

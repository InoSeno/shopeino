package db

import (
	"fmt"
	"net/url"
	"os"
	"shopeino/utils/config"

	"github.com/astaxie/beego/logs"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	Dbconn         *gorm.DB
	DatabaseString string
)

func init() {
	err := DbOpen()
	if err != nil {
		fmt.Println("err", err)
		logs.Error("error while open db , exiting...")
		os.Exit(500)
	}
}

func DbOpen() error {
	var err error
	db := config.Config.Db
	dbString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=%s", db.User, db.Pass, db.Host, db.Port, db.Name, url.QueryEscape(db.Location))
	DatabaseString = dbString

	Dbconn, err = gorm.Open("mysql", dbString)
	Dbconn.LogMode(db.Debug)
	if err != nil {
		return err
	}
	if errping := Dbconn.DB().Ping(); errping != nil {
		return errping
	}
	return nil
}

func GetDbConn(logID string) *gorm.DB {
	if errping := Dbconn.DB().Ping(); errping != nil {
		logs.Error("[%v] Db Not Connect test Ping :", logID, errping)
		errping = nil
		if errping = DbOpen(); errping != nil {
			logs.Error("[%v] try to connect again but error :", logID, errping)
		}
	}
	return Dbconn
}

package main

import (
	"runtime"
	_ "shopeino/routers"
	"shopeino/utils/config"

	"github.com/astaxie/beego/logs"

	"github.com/astaxie/beego"
)

func main() {

	runtime.GOMAXPROCS(1)
	app := config.Config.App
	beego.BConfig.AppName = app.AppName
	beego.BConfig.Listen.HTTPPort = app.HTTPPort
	beego.BConfig.WebConfig.AutoRender = app.AutoRender
	beego.BConfig.CopyRequestBody = app.CopyRequestBody
	beego.BConfig.WebConfig.EnableDocs = app.EnableDocs

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	logs.SetLogger(logs.AdapterFile, `{"filename":"logs/`+beego.BConfig.AppName+`.log","level":6,"daily":true,"maxdays":15,"perm":"0755","color":true}`)
	logs.Async()
	logs.EnableFuncCallDepth(true)
	logs.SetLogFuncCallDepth(3)

	beego.Run()
}

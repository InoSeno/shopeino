package models

import (
	"shopeino/db"
)

type Inventory struct {
	Id    int64   `json:"id" gorm:"column:id"`
	Sku   string  `json:"sku" gorm:"column:sku"`
	Name  string  `json:"name" gorm:"column:name"`
	Price float64 `json:"price" gorm:"column:price"`
	Qty   int     `json:"qty" gorm:"column:qty"`
	Promo int     `json:"promo" gorm:"column:promo"`
}

func (Inventory) TableName() string {
	return "inventory"
}


func (a *Inventory) SelectByName(logID string) (result Inventory, err error) {
	db := db.GetDbConn(logID)
	err = db.Where(&a).Find(&result).Error
	return
}

func (a *Inventory) SelectBySku(logID string) (result Inventory, err error) {
	db := db.GetDbConn(logID)
	err = db.Where(&a).Find(&result).Error
	return
}

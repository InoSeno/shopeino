package models

import (
	"shopeino/db"
)

type Promo struct {
	Id         int    `json:"id" gorm:"column:id"`
	Promotion  string `json:"promotion" gorm:"column:promotion"`
	Freesku    string `json:"freeSku" gorm:"column:freeSku"`
	Minqty     int    `json:"minqty" gorm:"column:minqty"`
	Pay        int    `json:"pay" gorm:"column:pay"`
	PercentOff float64    `json:"percentOff" gorm:"column:percentOff"`
}

func (Promo) TableName() string {
	return "promo"
}

func (a *Promo) SelectById(logID string) (result Promo, err error) {
	db := db.GetDbConn(logID)
	err = db.Where(&a).Find(&result).Error
	return
}

package models

type CheckoutRequest struct {
	Banyaknya  int    `json:"banyaknya"`
	NamaBarang string `json:"namaBarang"`
}

type CheckoutResponse struct {
	Banyaknya   int     `json:"banyaknya"`
	NamaBarang  string  `json:"namaBarang"`
	HargaSatuan float64 `json:"hargaBarang"`
	Promo       struct {
		NamaPromo        string `json:"namaPromo"`
		Free string    `json:"free"`
		PercentOff float64    `json:"percentOff"`
	} `json:"promo"`
	Total float64 `json:"total"`
}

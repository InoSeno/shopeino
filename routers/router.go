// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"shopeino/controllers"
	"shopeino/utils/logger"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"OPTIONS", "POST", "PUT", "GET", "DELETE", "PATCH"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type", "Access-Control-Allow-Origin"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))

	beego.InsertFilter("/*", beego.BeforeRouter, logger.LogsRequest, false)
	beego.InsertFilter("/*", beego.FinishRouter, logger.LogsResponse, false)

	ns := beego.NewNamespace("/shopeino/",
		beego.NSNamespace("/v1/",
			beego.NSRouter("/checkout", &controllers.Checkout{}, "post:CheckOut"),
		),
	)

	beego.AddNamespace(ns)
}

package logger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/logs"
	"github.com/tidwall/gjson"
)

var LogsRequest = func(c *context.Context) {
	t := time.Now().UnixNano() / int64(time.Millisecond)
	c.ResponseWriter.Header().Set("Timestamp", fmt.Sprintf(`%v`, t))
	time := c.ResponseWriter.Header().Get("Timestamp")
	b := new(bytes.Buffer)
	json.Compact(b, c.Input.RequestBody)

	c.Request.Header.Set("AppsId", getAppsId(c))

	picture1 := gjson.Get(fmt.Sprintf(`%v`, b), "id_card_picture")
	picture2 := gjson.Get(fmt.Sprintf(`%v`, b), "selfie_picture")
	body := fmt.Sprintf(`%v`, b)
	if picture1.String() != "" {
		body = strings.Replace(body, picture1.String(), "***thisIsBase64***", -1)
	}
	if picture2.String() != "" {
		body = strings.Replace(body, picture2.String(), "***thisIsBase64***", -1)
	}
	logs.Info("ID [%v] | Request - URL %s : %v | Header : %s | Body : %s ", time, c.Request.Method, c.Input.URI(), c.Request.Header, body)

}

var LogsResponse = func(c *context.Context) {
	t := c.ResponseWriter.Header().Get("Timestamp")
	b, _ := json.Marshal(c.Input.GetData("json"))
	logs.Info("ID [%v] | Response - URL %s : %v | Body : %s", t, c.Request.Method, c.Input.URI(), string(b))
}

func getAppsId(c *context.Context) (result string) {
	b := new(bytes.Buffer)
	json.Compact(b, c.Input.RequestBody)
	appsId := c.Request.Header.Get("AppsId")
	if appsId == "" {
		value := gjson.Get(fmt.Sprintf(`%v`, b), "AppsIdnonHeader")
		appsId = value.String()
	}
	result = appsId
	return
}

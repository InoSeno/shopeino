package common

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/astaxie/beego/logs"
	"github.com/tidwall/gjson"
	"gopkg.in/resty.v1"
)

func HttpRequest(method, url string, headers, params Ms, body interface{}, timeout int, logId string) (resp *resty.Response, err error) {
	client := resty.New().SetTimeout(time.Duration(timeout) * time.Millisecond)
	var r = client.R().SetHeaders(headers).SetQueryParams(params)
	switch method {
	case http.MethodGet:
		resp, err = r.Get(url)
	case http.MethodPost:
		resp, err = r.SetBody(body).Post(url)
	case http.MethodPatch:
		resp, err = r.SetBody(body).Patch(url)
	case http.MethodDelete:
		resp, err = r.SetBody(body).Delete(url)
	case http.MethodPut:
		resp, err = r.SetBody(body).Put(url)
	}

	// marshal body for logging
	a, _ := json.Marshal(r.Body)
	b := new(bytes.Buffer)
	json.Compact(b, a)

	picture1 := gjson.Get(fmt.Sprintf(`%v`, b), "id_card_picture")
	picture2 := gjson.Get(fmt.Sprintf(`%v`, b), "selfie_picture")
	bodyPrint := fmt.Sprintf(`%v`, b)

	if picture1.String() != "" {
		bodyPrint = strings.Replace(bodyPrint, picture1.String(), "***thisIsBase64***", -1)
	}

	if picture2.String() != "" {
		bodyPrint = strings.Replace(bodyPrint, picture2.String(), "***thisIsBase64***", -1)
	}
	logs.Info("ID [%s] | Request - URL %s : %s | Header : %s | Body : %s", logId, r.Method, r.URL, r.Header, bodyPrint)
	logs.Info("ID [%s] | Response -  URL %s :  %s | Status : %d | Body : %s | Time : %s", logId, r.Method, r.URL, resp.StatusCode(), resp.Body(), resp.Time())

	return
}

func HttpRequestV2(method, url string, headers http.Header, params Ms, body interface{}, timeout int, logId string) (resp *resty.Response, httpCode int, err error) {
	client := resty.New().SetTimeout(time.Duration(timeout) * time.Millisecond)
	var r = client.R().SetQueryParams(params)

	// Set header
	for h, val := range headers {
		r.Header[h] = val
	}
	if headers["Content-Type"] == nil {
		r.Header.Set("Content-Type", "application/json")
	}

	switch method {
	case http.MethodGet:
		resp, err = r.Get(url)
	case http.MethodPost:
		resp, err = r.SetBody(body).Post(url)
	case http.MethodPatch:
		resp, err = r.SetBody(body).Patch(url)
	case http.MethodDelete:
		resp, err = r.SetBody(body).Delete(url)
	case http.MethodPut:
		resp, err = r.SetBody(body).Put(url)
	}

	httpCode = resp.StatusCode()
	// marshal body for logging
	a, _ := json.Marshal(r.Body)
	b := new(bytes.Buffer)
	json.Compact(b, a)

	picture1 := gjson.Get(fmt.Sprintf(`%v`, b), "id_card_picture")
	picture2 := gjson.Get(fmt.Sprintf(`%v`, b), "selfie_picture")
	bodyPrint := fmt.Sprintf(`%v`, b)

	if picture1.String() != "" {
		bodyPrint = strings.Replace(bodyPrint, picture1.String(), "***thisIsBase64***", -1)
	}

	if picture2.String() != "" {
		bodyPrint = strings.Replace(bodyPrint, picture2.String(), "***thisIsBase64***", -1)
	}
	logs.Info("ID [%s] | Request - URL %s : %s | Header : %s | Body : %s", logId, r.Method, r.URL, r.Header, bodyPrint)
	logs.Info("ID [%s] | Response -  URL %s :  %s | Status : %d | Body : %s | Time : %s", logId, r.Method, r.URL, resp.StatusCode(), resp.Body(), resp.Time())

	return
}

func HttpXmlRequest(method, url string, headers http.Header, params Ms, body interface{}, timeout int, logId string) (resp *resty.Response, httpCode int, err error) {
	client := resty.New().SetTimeout(time.Duration(timeout) * time.Millisecond)
	var r = client.R().SetQueryParams(params)

	// Set header
	for h, val := range headers {
		r.Header[h] = val
	}

	if headers["Content-Type"] == nil {
		r.Header.Set("Content-Type", "application/xml")
	}

	resp, err = r.SetBody(body).Post(url)

	httpCode = resp.StatusCode()
	// marshal body for logging
	a, _ := xml.Marshal(r.Body)

	bodyPrint := string(a)

	logs.Info("ID [%s] | Request - URL %s : %s | Header : %s | Body : %s", logId, r.Method, r.URL, r.Header, bodyPrint)
	logs.Info("ID [%s] | Response -  URL %s :  %s | Status : %d | Body : %s | Time : %s", logId, r.Method, r.URL, resp.StatusCode(), resp.Body(), resp.Time())

	return
}

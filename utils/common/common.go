package common

import (
	"fmt"
	"shopeino/utils/constant"
	Error "shopeino/utils/error"
	"time"
)

type M map[string]interface{}
type Ms map[string]string

type Response struct {
	Code    int         `json:"code"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

const (
	FORMAT_DATETIME = "2006-01-02 15:04:05"
)

func BuildResponse(data interface{}, err error) (res *Response) {
	res = &Response{}
	res.Data = data
	if err != nil {
		if he, ok := err.(*Error.ApplicationError); ok {
			res.Code = he.ErrorCode
			res.Status = he.Status
			res.Message = err.Error()
			return
		} else {
			res.Code = constant.UNDEFINED_CODE
			res.Status = constant.UNDEFINED_STATUS
			res.Message = err.Error()
			return
		}
	}
	res.Code = constant.SUCCESS_CODE
	res.Status = constant.SUCCESS_STATUS

	return
}

func IsExpired(format, expDate string) (expired bool) {

	date := time.Now().Format(format)
	fmt.Println("Now: ", date)
	now, _ := time.Parse(format, date)
	exp, _ := time.Parse(format, expDate)
	fmt.Println("Exp date ", exp)
	diff := exp.Sub(now)

	fmt.Println("total", int(diff.Seconds()))
	fmt.Println("second", int(int(diff.Seconds())%60))

	second := int(diff.Seconds())

	if second < 0 {
		expired = true
	} else {
		expired = false
	}
	return expired
}

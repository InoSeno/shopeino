package redis

import (
	"encoding/json"
	"fmt"
	"swc-mobile-nano-backend/utils/config"
	"time"

	"github.com/astaxie/beego/logs"
	"github.com/go-redis/redis"
)

type Redis struct {
	*redis.Client
}

var (
	ClientRedis *redis.Client
)

type OptionRedis struct {
	Address  string `json:"address"`
	Password string `json:"password"`
	Database int    `json:"database"`
}

func init() {
	r := config.Config.Redis
	//redisType, _ := beego.AppConfig.Int("Redis.Type")
	option := OptionRedis{
		Address:  r.Addr,
		Database: r.Db,
		Password: r.Pass,
	}

	ClientRedis = NewRedis(option).Client
}

func NewRedis(option OptionRedis) *Redis {
	client := redis.NewClient(&redis.Options{
		Addr:     option.Address,
		Password: option.Password,
		DB:       option.Database,
	})

	_, error := client.Ping().Result()
	if error != nil {
		panic("Failed to connect redis")
	}

	return &Redis{client}
}

/*
 Redis Standard Get
*/
func (r *Redis) GetRedisKey(key string) (string, int) {
	fmt.Printf(" KEY : %s\n", key)
	val2, err := ClientRedis.Get(key).Result()
	if err == redis.Nil {
		logs.Info("Get Key " + key + " Key Not Found")
		return val2, 1
	} else if err != nil {
		logs.Info("Get Key "+key+" Error %v", err)
		return val2, -1

	} else {
		logs.Info("Get Key "+key+" Success %s", val2)
		return val2, 0

	}

}

/*
 Redis Standard Set
*/
func (r *Redis) SaveRedis(key string, val interface{}) error {
	var err error
	for i := 0; i < 3; i++ {
		err := ClientRedis.Set(key, val, 0).Err()
		if err == nil {
			break
		}
	}
	return err
}

/*
 Redis Standard Set Expired
*/
func (r *Redis) SaveRedisExp(key string, menit string, val interface{}) error {
	var err error
	data, _ := json.Marshal(val)
	for i := 0; i < 3; i++ {
		duration, _ := time.ParseDuration(menit)
		fmt.Printf("SET KEY : %v\n", key)

		fmt.Printf("SET duration : %v\n", duration)

		err := ClientRedis.Set(key, data, duration).Err()
		if err == nil {
			break
		}
		logs.Error("Error Redis ", err)
	}
	return err
}

func (r *Redis) SaveRedisExpNoMarshal(key string, menit string, val string) error {
	var err error
	for i := 0; i < 3; i++ {
		duration, _ := time.ParseDuration(menit)
		fmt.Printf("SET KEY : %v\n", key)

		fmt.Printf("SET duration : %v\n", duration)

		err := ClientRedis.Set(key, val, duration).Err()
		if err == nil {
			break
		}
		logs.Error("Error Redis ", err)
	}
	return err
}

/*
 Redis Standard Delete
*/
func (r *Redis) DeleteRedisKey(key string) error {
	var err error
	for i := 0; i < 3; i++ {
		err := ClientRedis.Del(key).Err()
		if err == nil {
			break
		}
	}
	return err
}

package constant

//Key String
const (
	AppsId          = "Apps-ID"
	AppsIdnonHeader = "apps_id"
	Auth            = "Authorization"
	InstitutionId   = "Institution-ID"
	Signature       = "Signature"
	Timestamp       = "Timestamp"
	InquiryType     = "type"
)

//Resp Code
const (
	SUCCESS_CODE            = 200
	BAD_REQUEST_CODE        = 400
	UNAUTHORIZED_CODE        = 401
    FORBIDDEN_CODE  		= 403
	UNPROCESSABLE_ENTITY_CODE = 422
	UNDEFINED_CODE = 500
)

//Resp Status
const (
	SUCCESS_STATUS   = "SUCCESS"
	PENDING_STATUS   = "PENDING"
	FAILED_STATUS    = "FAILED"
	UNDEFINED_STATUS = "ERROR"
)


var RcEnum = newResponseRegistry()

type ResponseCode struct {
	Code    int
	Status  string
	Message string
}

var (
	Forbidden = &ResponseCode{FORBIDDEN_CODE, FAILED_STATUS, "Forbidden"}
	Bad_Request = &ResponseCode{BAD_REQUEST_CODE, FAILED_STATUS, "Bad Request"}
	Unprocessable_Entity = &ResponseCode{UNPROCESSABLE_ENTITY_CODE, FAILED_STATUS, "Email Already Registered"}
	Undefined = &ResponseCode{FORBIDDEN_CODE, UNDEFINED_STATUS, "Undefined"}
	Unauthorized = &ResponseCode{UNAUTHORIZED_CODE, FAILED_STATUS, "Unauthorized"}

)

func (c *ResponseCode) Int() int {
	return c.Code
}

func newResponseRegistry() *responseRegistry {

	return &responseRegistry{
		response: []*ResponseCode{Forbidden,Bad_Request,Undefined,Unprocessable_Entity},
	}
}

type responseRegistry struct {
	response []*ResponseCode
}

func (c *responseRegistry) List() []*ResponseCode {
	return c.response
}

func (c *responseRegistry) Get(s int) *ResponseCode {
	for _, response := range c.List() {
		if response.Int() == s {
			return response
		}
	}
	return Undefined
}

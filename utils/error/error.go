package error

import (
	"os"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func New(errorCode int, status, message string) error {
	return &ApplicationError{
		ErrorCode: errorCode,
		Status:    status,
		Message:   message,
	}
}

type ApplicationError struct {
	ErrorCode int
	Status    string
	Message   string
}

func (e *ApplicationError) Error() string {
	return e.Message
}

// handling timeout
func IsTimeout(err error) (timeout bool) {
	timeout = os.IsTimeout(err)
	if timeout {
		return
	}

	st, ok := status.FromError(err)
	if !ok {
		return
	}

	if st.Code() == codes.DeadlineExceeded {
		timeout = true
	}

	return
}

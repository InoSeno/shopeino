package config

import (
	"os"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/spf13/viper"
)

func init() {
	InitConfig()
}

type App struct {
	AppName         string `json:"appName"`
	HTTPPort        int    `json:"httpPort"`
	AutoRender      bool   `json:"autoRender"`
	CopyRequestBody bool   `json:"copyRequestBody"`
	EnableDocs      bool   `json:"enableDocs"`
}

type Db struct {
	Host     string `json:"host"`
	DbType   string `json:"dbType"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Name     string `json:"name"`
	Pass     string `json:"pass"`
	Location string `json:"location"`
	SslMode  string `json:"sslMode"`
	Alias    string `json:"alias"`
	Force    bool   `json:"force"`
	Verbose  bool   `json:"verbose"`
	Debug    bool   `json:"debug"`
}

type Smtp struct {
	MailHost     string `json:"mailHost"`
    MailPort     int `json:"mailPort"`
	MailUserName     string  `json:"mailUserName"`
    MailPassword     string `json:"mailPassword"`
	MailEncryption     string  `json:"mailEncryption"`
	MailFromAddress     string  `json:"mailFromAddress"`
	MailSubject string `json:"mailSubject"`
}

type Configuration struct {
	App App `json:"app"`
	Db  Db  `json:"db"`
	Smtp Smtp `json:"smtp"`
}

var Config Configuration

func InitConfig() {

	beego.BConfig.RunMode = "dev"
	if len(os.Args) > 1 {
		beego.BConfig.RunMode = os.Args[1]
	}

	path := "./conf"
	name := "config." + beego.BConfig.RunMode
	if strings.Contains(beego.BConfig.RunMode, "-test") {
		dir, _ := os.Getwd()
		dir = strings.Replace(dir, after(dir, "swc-mobile-nano-backend"), "", -1)
		path = dir + "/conf"
		name = "config.test"
	}

	viper.AddConfigPath(path)
	viper.SetConfigName(name)
	viper.SetConfigType("json")

	err := viper.ReadInConfig()

	if err != nil {
		logs.Error("Error Load Config : " + err.Error())
		panic(err.Error())
		os.Exit(1)
	}

	Config = Configuration{}
	err = viper.Unmarshal(&Config)
	if err != nil {
		logs.Error("Error Unmarshal JSON Config : " + err.Error())
	}
}

func after(value string, a string) string {
	// Get substring after a string.
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

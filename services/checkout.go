package services

import (
	"shopeino/models"
	"shopeino/utils/constant"
	Error "shopeino/utils/error"
	"fmt"

	"encoding/json"

	"github.com/astaxie/beego/logs"
)

func Checkout(request []byte, logId string) (response interface{}, err error) {
	var reqBody models.CheckoutRequest

	err = json.Unmarshal(request, &reqBody)
	if err != nil {
		logs.Error("ID [%s] | %s", logId, err.Error())
		err = Error.New(constant.Bad_Request.Code, constant.Bad_Request.Status, constant.Bad_Request.Message)
		return
	}

	inventory := models.Inventory{
		Name: reqBody.NamaBarang,
	}

	inventorySelected, err := inventory.SelectByName(logId)
	if err != nil {
		logs.Error("ID [%s] | %s", logId, err.Error())
		return
	}
	if inventorySelected.Qty < reqBody.Banyaknya {
		logs.Error("ID [%s] | %s", logId, err.Error())
		err = Error.New(constant.UNDEFINED_CODE, constant.FAILED_STATUS, "pemesanan melebihi qty yg ada di inventory")
		return
	}

	fmt.Println("inventorySelected",inventorySelected)

	promo := models.Promo{
		Id: inventorySelected.Promo,
	}

	promoSelected, err := promo.SelectById(logId)
	if err != nil {
		logs.Error("ID [%s] | %s", logId, err.Error())
		return
	}
	fmt.Println("promoSelected",promoSelected)

	free := models.Inventory{
		Sku: promoSelected.Freesku,
	}

	freeSelected, err := free.SelectBySku(logId)
	if err != nil {
		if err.Error() != "record not found" {
			logs.Error("ID [%s] | %s", logId, err.Error())
			return
		}
	}
	fmt.Println("freeSelected",freeSelected)

	checkOutResponse := models.CheckoutResponse{
		Banyaknya:   reqBody.Banyaknya,
		NamaBarang:  reqBody.NamaBarang,
		HargaSatuan: inventorySelected.Price,
	}
	checkOutResponse.Promo.NamaPromo = promoSelected.Promotion

	if promoSelected.Freesku != "" {

		checkOutResponse.Promo.Free = freeSelected.Name
	}
	if promoSelected.PercentOff > 0 {
		checkOutResponse.Promo.PercentOff = promoSelected.PercentOff
	}

	if promoSelected.Minqty > 0 && promoSelected.Pay > 0 {
		checkOutResponse.Total = inventorySelected.Price * float64(promoSelected.Pay)
	} else { 
		checkOutResponse.Total = inventorySelected.Price * float64(reqBody.Banyaknya)
	}

	fmt.Println("inventorySelected.Price",inventorySelected.Price)
	fmt.Println("reqBody.Banyaknya",reqBody.Banyaknya)
	fmt.Println("checkOutResponse.Total",checkOutResponse.Total)
	fmt.Println("promoSelected.PercentOff",promoSelected.PercentOff/100)
	if promoSelected.PercentOff > 0 {
		checkOutResponse.Total = checkOutResponse.Total - (checkOutResponse.Total * float64(promoSelected.PercentOff/100))
	}

	response = checkOutResponse

	return
}

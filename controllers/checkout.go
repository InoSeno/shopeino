package controllers

import (
	"shopeino/services"
	"shopeino/utils/common"
	"shopeino/utils/constant"

	"github.com/astaxie/beego"
)

type Checkout struct {
	beego.Controller
}

func (c *Checkout) CheckOut() {
	t := c.Ctx.ResponseWriter.Header().Get(constant.Timestamp)

	reqBody := c.Ctx.Input.RequestBody
	response, err := services.Checkout(reqBody, t)
	resp := common.BuildResponse(response, err)

	c.Data["json"] = resp
	c.ServeJSON()
}
